/*
 * Copyright (c) 2013, 2020, Boomerang Retail Pvt Ltd. All rights reserved.
 * BOOMERANG PROPERTY CONFIDENTIAL. Use is subject to license terms.
 */

package org.flywaydb.core.internal.dbsupport.snowflake;


import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.JdbcTemplate;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.flywaydb.core.internal.dbsupport.Table;
import org.flywaydb.core.internal.util.jdbc.JdbcUtils;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.ListIterator;

public class SnowflakeTable extends Table {
    private static final Log LOG = LogFactory.getLog(SnowflakeTable.class);

    /**
     * Creates a new PostgreSQL table.
     *
     * @param jdbcTemplate The Jdbc Template for communicating with the DB.
     * @param dbSupport    The database-specific support.
     * @param schema       The schema this table lives in.
     * @param name         The name of the table.
     */
    public SnowflakeTable(JdbcTemplate jdbcTemplate, SnowflakeDBSupport dbSupport, SnowflakeSchema schema, String name) {
        super(jdbcTemplate, dbSupport, schema, name);
    }

    @Override
    protected void doDrop() throws SQLException {
        jdbcTemplate.execute("DROP TABLE " + dbSupport.quote(schema.getName(), name) + " CASCADE");
    }

    @Override
    protected boolean doExists() throws SQLException {
        return exists(null, schema, name);
    }


    /**
     * Checks whether the database contains a table matching these criteria.
     *
     * @param catalog    The catalog where the table resides. (optional)
     * @param schema     The schema where the table resides. (optional)
     * @param table      The name of the table. (optional)
     * @param tableTypes The types of table to look for (ex.: TABLE). (optional)
     * @return {@code true} if a matching table has been found, {@code false} if not.
     * @throws SQLException when the check failed.
     */
    protected boolean exists(Schema catalog, Schema schema, String table, String... tableTypes) throws SQLException {
        try {

            String schemaName = catalog == null ? schema.getName() : dbSupport.quote(catalog.getName(), schema.getName());
            LOG.info("Checking for existence of table [" + table+"] in schema [" + schema +"]");
            String query = "show tables like '" + table +"' in " + schemaName;
            //String query = "select count(*) from " + dbSupport.quote(schema.getName(), table);
            List <String> tableNamesList = jdbcTemplate.queryForStringList(query, 2);
            for (ListIterator <String> iterator = tableNamesList.listIterator(); iterator.hasNext();) {
                iterator.set(iterator.next().toLowerCase());
            }

            //int value = jdbcTemplate.queryForInt(query);
            LOG.info("Found tables " + tableNamesList+" in schema [" + schema +"] matching pattern [" + table +"]");
            //return value >= 0;
            boolean matchFound =  tableNamesList!= null && tableNamesList.contains(table.toLowerCase());
            LOG.info("Match Found [" + matchFound +"]");
            return matchFound;
        }catch(SQLException e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    protected void doLock() throws SQLException {
        //jdbcTemplate.execute("SELECT * FROM " + this + " FOR UPDATE");
    }
}
