/*
 * Copyright (c) 2013, 2020, Boomerang Retail Pvt Ltd. All rights reserved.
 * BOOMERANG PROPERTY CONFIDENTIAL. Use is subject to license terms.
 */

package org.flywaydb.core.internal.dbsupport.snowflake;


import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.JdbcTemplate;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.flywaydb.core.internal.dbsupport.SqlStatementBuilder;
import org.flywaydb.core.internal.dbsupport.postgresql.PostgreSQLSqlStatementBuilder;
import org.flywaydb.core.internal.util.StringUtils;
import org.flywaydb.core.internal.util.logging.Log;
import org.flywaydb.core.internal.util.logging.LogFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class SnowflakeDBSupport extends DbSupport {
    private static final Log LOG = LogFactory.getLog(SnowflakeDBSupport.class);

    /**
     * Creates a new instance.
     *
     * @param connection The connection to use.
     */
    public SnowflakeDBSupport (Connection connection) {
        super(new JdbcTemplate(connection, Types.NULL));
    }

    public String getDbName() {
        return "snowflake";
    }

    public String getCurrentUserFunction() {
        return "current_user";
    }

    @Override
    public Schema getOriginalSchema() {
        if (originalSchema == null) {
            return null;
        }

        return getSchema(this.originalSchema);
    }

    @Override
    protected String doGetCurrentSchemaName() throws SQLException {
        return jdbcTemplate.queryForString("SELECT current_schema();");
    }

    @Override
    public void changeCurrentSchemaTo(Schema schema) {
        if (schema.getName().equals(originalSchema) || !schema.exists()) {
            return;
        }

        try {
            doChangeCurrentSchemaTo(schema.toString());
        } catch (SQLException e) {
            throw new FlywayException("Error setting current schema to " + schema, e);
        }
    }

    @Override
    protected void doChangeCurrentSchemaTo(String schema) throws SQLException {
        if (!StringUtils.hasLength(schema)) {
            return;
        }
        String currentDB = jdbcTemplate.queryForString("select CURRENT_DATABASE()");
        LOG.info("currentDB = " + currentDB + " and schema " + schema);
        jdbcTemplate.execute("use schema " + schema);
    }

    public boolean supportsDdlTransactions() {
        return true;
    }

    public String getBooleanTrue() {
        return "TRUE";
    }

    public String getBooleanFalse() {
        return "FALSE";
    }

    public SqlStatementBuilder createSqlStatementBuilder() {
        return new PostgreSQLSqlStatementBuilder();
    }

    @Override
    public String doQuote(String identifier) {
        //Removing the quote as
        //return "\"" + StringUtils.replaceAll(identifier, "\"", "\"\"") + "\"";
        return identifier;
    }

    @Override
    public Schema getSchema(String name) {
        return new SnowflakeSchema(jdbcTemplate, this, name);
    }

    @Override
    public boolean catalogIsSchema() {
        return false;
    }

    public boolean isCaseSensitive () {return true;}
}
